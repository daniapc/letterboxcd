# Projeto: letterboxcd

## Equipe: letterboxcd

## Descrição: 

Análise do perfil de interesses cinematográficos da comunidade curitibana de usuários da rede social Letterbox, a partir das características descritivas dos filmes e do desempenho em avaliações. Ainda, pretende-se comparar as preferências de filmes da rede local com as preferências de filmes da comunidade global. Complementando essa análise, com base em dados externos a Letterbox, busca-se traçar o impacto de premiações de filmes em cerimônias de cinema nas avaliações dos filmes das comunidades local e global do Letterbox.

## Membros:

Bernardo Vendruscolo Mendes, 2355779, berWoW, EC, UTFPR

Daniel Augusto Pires de Castro, 2240246, daniapc, EC, UTFPR

Viviane Ruotolo, 2324822, vivianeruotolo, EC, UTFPR