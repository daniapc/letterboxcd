import pandas as pd
import os

df_res = pd.DataFrame()

movies = {}

for name in os.listdir('scraper_outputs'):
    if name.endswith('.csv'):
        df = pd.read_csv(f'scraper_outputs/{name}')
        for index, row in df.iterrows():
            if row['Film_title'] in movies:
                df_res.loc[movies[row['Film_title']], 'Curitiba_watches'] += 1
                if not pd.isna(row['Owner_rating']):
                    if pd.isna(df_res.loc[movies[row['Film_title']], 'Curitiba_average_rating']):
                        df_res.loc[movies[row['Film_title']], 'Curitiba_average_rating'] = row['Owner_rating']
                        df_res.loc[movies[row['Film_title']], 'Curitiba_ratings'] = 1
                    else:
                        df_res.loc[movies[row['Film_title']], 'Curitiba_average_rating'] += row['Owner_rating']
                        df_res.loc[movies[row['Film_title']], 'Curitiba_ratings'] += 1
            else:
                movies[row['Film_title']] = len(movies)
                df_row = pd.DataFrame(row).transpose()
                
                df_row.rename(columns = {'Owner_rating': 'Curitiba_average_rating'}, inplace = True)
                df_row.insert(27, 'Curitiba_ratings', 1 if not pd.isna(row['Owner_rating']) else 0)
                df_row.insert(13, 'Curitiba_watches', 1)
                df_row.insert(0, 'Index', movies[row['Film_title']])
                df_row.set_index('Index', inplace = True)
                
                df_res = pd.concat([df_res, df_row])
                
for index, row in df_res.iterrows():
    if row['Curitiba_ratings'] != 0:
        df_res.loc[index, 'Curitiba_average_rating'] = row['Curitiba_average_rating'] / row['Curitiba_ratings']

df_res.to_csv('../../../data/letterboxd_Curitiba.csv', index=False)

df_res
