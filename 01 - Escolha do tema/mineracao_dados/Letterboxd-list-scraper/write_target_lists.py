file = open("../letterboxd-profiles-scraper/merge_6", "r")

target_list = []

for line in file:
    target_list.append(line.strip())

file = open("curitiba_target_lists.txt", "w")

for target in target_list:
    file.write(f"https://letterboxd.com/{target}/films/\n")
