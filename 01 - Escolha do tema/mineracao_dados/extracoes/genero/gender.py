import pandas as pd
import unidecode

class Gender:
    def __init__(
            self,
            data_path = ""):
        
        self.data_path = data_path

    def hello(self):

        print("Hello world!")

    def save_names_from_data(self, path):

        data_path = self.data_path
        df = pd.read_csv(data_path)
        df = df["Director"]

        names_list = []

        for director in df:
            #deixa minusculo e remove pontos
            director = str(director).lower().replace(".","")
            #remove acento
            director = unidecode.unidecode(director)

            names = director.split(", ")

            for name in names:
                # instances.append([name, "False"])
                names_list.append(name)
                # verified_list.append("False")

        names_list = list(dict.fromkeys(names_list))
        
        df_ja_existente = pd.read_csv(path)
        list_ja_existente = df_ja_existente.values.tolist()

        for instance in list_ja_existente:
            if instance[0] in names_list:
                names_list.remove(instance[0])
        names_list.remove('nan')

        instances = []
        for name in names_list:
            instances.append([name, "False"]) 

        df = pd.DataFrame(instances, columns=['nome', 'verificado'])

        # saving the dataframe
        # df.to_csv(path, index=False)            
        df.to_csv(path, mode='a', index=False, header=False)            

    def print_df(self, path):
        df = pd.read_csv(path)
        print(df)

    def extract_gender_from_dataset(self, name_path, dst_path, dataset_file):
        # Leitura do dataset de nome e conversão em dicionário
        df = pd.read_csv(dataset_file)
        dataset = df.values.tolist()

        gender_dict = {}
        for line in dataset:
            key = line[0].lower()
            value = line[1]
            if key not in gender_dict:
                gender_dict[key] = value
        
        # Leitura do arquivo dos nomes
        df = pd.read_csv(name_path)
        names = df.values.tolist()

        for name in names:
            if name[1] == "False":
                try:
                    first_name = name[0].split()[0]
                    if first_name not in gender_dict:
                        continue
                    gender = gender_dict[first_name]
                    
                    # Atualiza nome no arquivo de gênero
                    gender_df = pd.DataFrame([[name[0], gender]], columns=['name', 'gender'])
                    gender_df.to_csv(dst_path, mode='a', index=False, header=False)

                    # Atualiza como verificado
                    index = names.index(name)
                    df.loc[index, 'verificado'] = "True"
                    df.to_csv(name_path, index=False)
                except:
                    continue


    def extract_gender_from_site(self, name_path, dst_path):
        self.hello()
        from bs4 import BeautifulSoup
        import requests

        male_dict = [" he ", " him ", " his "]
        female_dict = [" she ", " her ", " hers "]

        # Leitura do arquivo dos nomes
        df = pd.read_csv(name_path)
        names = df.values.tolist()

        for name in names:
            if name[1] == "False":
                try:
                    gender = "None"

                    name_link = name[0].replace(" ", "-")

                    page_response = requests.get("https://letterboxd.com/director/" + name_link +"/")

                    if page_response.status_code != 200:
                        continue

                    page_soup = BeautifulSoup(page_response.content, 'lxml')
                    bio = page_soup.find("div", class_="bio").find("section", class_="section panel-text js-tmdb-bio")
                    bio = bio.contents

                    for content in bio:
                        if content == "\n" or content == None:
                            continue
                        lower_content = str(content).lower()
                        
                        for pronoun in male_dict:
                            if pronoun in lower_content:
                                gender = "M"
                                
                        for pronoun in female_dict:
                            if pronoun in lower_content:
                                gender = "F"
                    
                    # Atualiza nome no arquivo de gênero
                    gender_df = pd.DataFrame([[name[0], gender]], columns=['name', 'gender'])
                    gender_df.to_csv(dst_path, mode='a', index=False, header=False)

                    # Atualiza como verificado
                    index = names.index(name)
                    df.loc[index, 'verificado'] = "True"
                    df.to_csv(name_path, index=False)
                except:
                    continue

        # df = pd.read_csv(name_path)

        # df.to_csv(dst_path, mode='a', header=False)

    def make_director_gender_dataset(self, gender_path, dst_path):
        
        data_path = self.data_path
        df = pd.read_csv(data_path)
        df = df[["Film_title", "Director"]].values.tolist()

        gender_df = pd.read_csv(gender_path).values.tolist()
        gender_dict = {}
        for instance in gender_df:
            gender_dict[instance[0]] = str(instance[1])

        df_ja_existente = pd.read_csv(dst_path)["Film_title"]
        verificados = df_ja_existente.values.tolist()

        for instance in df:
            film_title = instance[0]

            if film_title in verificados:
                continue                

            director = instance[1]

            director = str(director).lower().replace(".","")
            #remove acento
            director = unidecode.unidecode(director)

            names = director.split(", ")

            gender_list = []
            for name in names:
                if name in gender_dict:
                    gender_list.append(gender_dict[name])
            f_director_count = gender_list.count("F")
            m_director_count = gender_list.count("M")

            # Atualiza nome no arquivo de gênero Film_title,Director_gender
            director_gender_df = pd.DataFrame(
                [[film_title, f_director_count, m_director_count]], 
                columns=['Film_title', 'Female_director_count', 'Male_director_count'])
            director_gender_df.to_csv(dst_path, mode='a', index=False, header=False)

    def extract_gender_from_dataset_to_cast(self, dataset_file, dst_path):
        
        data_path = self.data_path
        cast_df = pd.read_csv(data_path)
        cast_df = cast_df[["Film_title", "Cast"]].values.tolist()

        # Leitura do dataset de nome e conversão em dicionário
        df = pd.read_csv(dataset_file)
        dataset = df.values.tolist()

        gender_dict = {}
        for line in dataset:
            key = line[0].lower()
            value = line[1]
            if key not in gender_dict:
                gender_dict[key] = value

        df_ja_existente = pd.read_csv(dst_path)["Film_title"]
        verificados = df_ja_existente.values.tolist()

        for instance in cast_df:
            film_title = instance[0]

            if film_title in verificados:
                continue                

            actor = instance[1]

            actor = str(actor).lower().replace(".","").replace("[", "").replace("]", "").replace("\'","")
            #remove acento
            actor = unidecode.unidecode(actor)

            names = actor.split(", ")

            gender_list = []
            for name in names:
                if name == "nan":
                    continue
                first_name = name.split()[0]
                if first_name in gender_dict:
                    gender_list.append(gender_dict[first_name])
            f_actor_count = gender_list.count("F")
            m_actor_count = gender_list.count("M")

            # Atualiza nome no arquivo de gênero Film_title,actor_gender
            actor_gender_df = pd.DataFrame(
                [[film_title, f_actor_count, m_actor_count]], 
                columns=['Film_title', 'Female_actor_count', 'Male_actor_count'])
            actor_gender_df.to_csv(dst_path, mode='a', index=False, header=False)