import pandas as pd

class Award:
    def __init__(
            self,
            data_path = ""):
        
        self.data_path = data_path

    def print_df(self, path):
        df = pd.read_csv(path)
        print(df)

    def contar_indicacoes(self, path, dst_path):

        data_path = self.data_path

        df = pd.read_csv(data_path)
        df = df["Film_title"]

        films = df.values.tolist()

        data = pd.read_csv(path)
        data = data["film"]

        indications = data.values.tolist()
        
        df_ja_existente = pd.read_csv(dst_path)["Film_title"]
        verificados = df_ja_existente.values.tolist()

        for film in films:
            if film not in verificados:
                count = indications.count(film)
                if count != 0:
                    indications_df = pd.DataFrame([[film, count]], columns=['Film_title', 'indications_count'])
                    indications_df.to_csv(dst_path, mode='a', index=False, header=False)

    def contar_oscars(self, path, dst_path):
        data_path = self.data_path

        df = pd.read_csv(data_path)
        df = df["Film_title"]

        films = df.values.tolist()

        data = pd.read_csv(path)
        data = data[["film", "winner"]]

        indications = data.values.tolist()

        df_ja_existente = pd.read_csv(dst_path)["Film_title"]
        verificados = df_ja_existente.values.tolist()

        for film in films:
            if film not in verificados:
                count = indications.count([film, True])
                if count != 0:
                    indications_df = pd.DataFrame([[film, count]], columns=['Film_title', 'victories_count'])
                    indications_df.to_csv(dst_path, mode='a', index=False, header=False)