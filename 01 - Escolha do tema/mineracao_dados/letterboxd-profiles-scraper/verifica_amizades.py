## esse arquivo verifica toda a lista de amizades fornecida do arquivo 

arquivo_curitibanos = "amizades_nao_curitibanas"
arquivo_amigos = "amizades_5"

from bs4 import BeautifulSoup
import requests

def adicionar_amizade(caminho, perfil):
    with open(caminho, "a") as file:
        file.write(perfil+"\n")

# amizades_verificadas = []

curitiba = []

# with open("amizades_verificadas") as file:
#     text = file.readlines()
#     for line in text:
#         amizades_verificadas.append(line[:-1])

with open(arquivo_curitibanos) as file:
    text = file.readlines()
    for line in text:
        curitiba.append(line[:-1])

for profile in curitiba:
    # profile = "daniapc"
    lenght = len(curitiba)
    print(str(curitiba.index(profile))+"/"+str(lenght)+": "+profile)

    # if profile not in amizades_verificadas:

    i = 1

    seguidores = ['null']

    # perfis = []

    while seguidores != []:
        page_response = requests.get("https://letterboxd.com/" + profile + "/followers/page/" + str(i) + "/")
        
        page_soup = BeautifulSoup(page_response.content, 'lxml')

        try:
            tabela = page_soup.find("table", class_="person-table")
            seguidores = tabela.find_all("a", class_="name")
        except:
            seguidores = []

        for seguidor in seguidores:
            adicionar_amizade(arquivo_amigos,seguidor.attrs['href'][1:-1])

            # print(seguidor.attrs['href'])

        i += 1
        
        # adicionar_amizade("amizades_verificadas",profile)
        # amizades_verificadas.append(profile)