from bs4 import BeautifulSoup
import requests

perfis = []

with open("curitiba") as file:
    texto = file.readlines()
    file_len = len(texto)
    i = 0

    for line in texto:
        print(str(i)+"/"+str(file_len))

        line = line[:-1]

        ## COLETA O PERFIL DA PESSOA NO LETTERBOXD E VERIFICA A LOCALIZAÇÃO ONDE ELA MORA {
        page_response = requests.get("https://letterboxd.com/" + line)
        
        # Check to see page was downloaded correctly
        if page_response.status_code != 200:
            continue

        page_soup = BeautifulSoup(page_response.content, 'lxml')

        lugar = page_soup.find("div", class_="metadatum -has-label js-metadatum")
        if lugar != None:
            lugar = lugar.find("span", class_="label").contents[0].lower()
            
            if "curitiba" in lugar:
                perfis.append(line)
        
        ## } FIM 

        i += 1

perfis = list(dict.fromkeys(perfis))

with open("curitiba", "w") as file:
    for perfil in perfis:
        file.write(perfil + "\n")