## esse arquivo verifica toda a lista de amizades e filtra se foi verificada ou nao alem de conferir localizacao

arquivo_curitibanos = "amizades_nao_curitibanas"
# arquivo_curitibanos = "merge_5"
arquivo_amigos = "amizades_8"
threshold_seg = 50
arquivo_amizades_verificadas = "amizades_verificadas_2"

from bs4 import BeautifulSoup
import requests

def verifica_quantidade(perfil):
    page_response = requests.get("https://letterboxd.com/" + perfil)

    if page_response.status_code == 200:
        page_soup = BeautifulSoup(page_response.content, 'lxml')
        lugar = page_soup.find("a", href="/"+perfil+"/followers/")
        valor = lugar.find("span", class_="value").contents[0]

        return str(valor).replace(",","")

    return -1


def verifica_localizacao(perfil):
    page_response = requests.get("https://letterboxd.com/" + perfil)
        
        # Check to see page was downloaded correctly
    if page_response.status_code == 200:
        
        page_soup = BeautifulSoup(page_response.content, 'lxml')
        lugar = page_soup.find("div", class_="metadatum -has-label js-metadatum")

        if lugar != None:
            lugar = lugar.find("span", class_="label").contents[0].lower()
            if "curitiba" in lugar:
                return perfil
    
    return None

def adicionar_amizade(caminho, perfil):
    with open(caminho, "a") as file:
        file.write(perfil+"\n")

amizades_verificadas = []

curitiba = []

with open(arquivo_amizades_verificadas) as file:
    text = file.readlines()
    for line in text:
        amizades_verificadas.append(line[:-1])

with open(arquivo_curitibanos) as file:
    text = file.readlines()
    for line in text:
        curitiba.append(line[:-1])

while (True):
    try: 
        for profile in curitiba:
            # profile = "daniapc"
            lenght = len(curitiba)
            print(str(curitiba.index(profile))+"/"+str(lenght)+": "+profile)

            if profile not in amizades_verificadas:

                i = 1

                quantidade = int(verifica_quantidade(profile))

                seguidores = ['null'] if quantidade < threshold_seg and quantidade != -1 else []


                # perfis = []

                while seguidores != []:
                    page_response = requests.get("https://letterboxd.com/" + profile + "/followers/page/" + str(i) + "/")
                    
                    page_soup = BeautifulSoup(page_response.content, 'lxml')

                    try:
                        tabela = page_soup.find("table", class_="person-table")
                        seguidores = tabela.find_all("a", class_="name")
                    except:
                        seguidores = []

                    for seguidor in seguidores:
                        result = verifica_localizacao(seguidor.attrs['href'][1:-1])
                        if result != None:
                            # perfis.append(result)
                            adicionar_amizade(arquivo_amigos,result)

                        # print(seguidor.attrs['href'])

                    i += 1
                
                adicionar_amizade(arquivo_amizades_verificadas,profile)
                amizades_verificadas.append(profile)
    except:
        print("Erro")


