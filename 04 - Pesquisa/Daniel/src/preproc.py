import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
class Preproc:
    def print_hello():
        print("Hello world!")

    def juntar_dados(type, src_dir):


        main_df = pd.read_csv(src_dir + "letterboxd_Curitiba.csv")
        films_title = main_df["Film_title"].values.tolist()

        director_gender_df = pd.read_csv(src_dir + "director_gender.csv")
        cast_gender_df = pd.read_csv(src_dir + "cast_gender.csv")
        indicacoes_df = pd.read_csv(src_dir + "indicacoes.csv")
        vitorias_df = pd.read_csv(src_dir + "vitorias.csv")

        main_df = main_df.merge(director_gender_df, how='left', left_on='Film_title', right_on='Film_title').fillna(0)
        main_df = main_df.merge(cast_gender_df, how='left',left_on='Film_title', right_on='Film_title').fillna(0)
        main_df = main_df.merge(indicacoes_df, how='left',left_on='Film_title', right_on='Film_title').fillna(0)
        main_df = main_df.merge(vitorias_df, how='left',left_on='Film_title', right_on='Film_title').fillna(0)

        main_df.to_csv("./data/data_" +type+".csv",index=False)

        print(main_df)

    def limpar_dados_brancos(path, coluna):
        df = pd.read_csv(path)
        df = df[df[coluna] != 0]

        df.to_csv(path, index=False)


    def vetorizar_dados_categoricos(path, dst):
        # categoricos: Genres, Countries, Original_language, Studios
        df = pd.read_csv(path)

        if 'Genres' in df.columns:
            new_genres = []
            genre_list = df['Genres'].values.tolist()
            for genre in genre_list:
                genre_names = str(genre).replace("[","").replace("]","").replace("\'","").split(', ')
                new_genres.append(genre_names)
            df['Genres'] = new_genres
            vect = CountVectorizer()
            X = vect.fit_transform(df.Genres.str.join(' '))
            feature_names_out = vect.get_feature_names_out()
            genres_columns = []
            for feature in feature_names_out:
                genres_columns.append("Genres_"+feature)
            df = df.join(pd.DataFrame(X.toarray(), columns=genres_columns))
            df = df.drop('Genres', axis=1)

        if 'Countries' in df.columns:
            new_genres = []
            genre_list = df['Countries'].values.tolist()
            for genre in genre_list:
                genre_names = str(genre).replace("[","").replace("]","").replace("\'","").split(', ')
                new_genres.append(genre_names)
            df['Countries'] = new_genres
            vect = CountVectorizer()
            X = vect.fit_transform(df.Countries.str.join(' '))
            feature_names_out = vect.get_feature_names_out()
            genres_columns = []
            for feature in feature_names_out:
                genres_columns.append("Countries_"+feature)
            df = df.join(pd.DataFrame(X.toarray(), columns=genres_columns))
            df = df.drop('Countries', axis=1)

        if 'Original_language' in df.columns:
            new_genres = []
            genre_list = df['Original_language'].values.tolist()
            for genre in genre_list:
                genre_names = str(genre).replace("[","").replace("]","").replace("\'","").split(', ')
                new_genres.append(genre_names)
            df['Original_language'] = new_genres
            vect = CountVectorizer()
            X = vect.fit_transform(df.Original_language.str.join(' '))
            feature_names_out = vect.get_feature_names_out()
            genres_columns = []
            for feature in feature_names_out:
                genres_columns.append("Original_language_"+feature)
            df = df.join(pd.DataFrame(X.toarray(), columns=genres_columns))
            df = df.drop('Original_language', axis=1)

        df.to_csv(dst, index=False)

    

    def clean_dataset(df):
        assert isinstance(df, pd.DataFrame), "df needs to be a pd.DataFrame"
        df.dropna(inplace=True)
        indices_to_keep = ~df.isin([np.nan, np.inf, -np.inf]).any(axis=1)
        return df[indices_to_keep].astype(np.float64)
