import pandas as pd
from preproc import Preproc

class Machine_learning:

    def isolation_forest(type, anomaly_threshold, importance_threshold):
        from sklearn.ensemble import IsolationForest

        # anomaly_threshold = 20
        # importance_threshold = 10

        path = "./data/data_if.csv"

        Preproc.limpar_dados_brancos(path, type+"_count")

        path_vetorizado = "./data/data_if_vetorizado.csv"

        Preproc.vetorizar_dados_categoricos(path, path_vetorizado)

        df = pd.read_csv(path_vetorizado)
        # df = pd.read_csv("./data/gender.csv")
        X_train = df.drop(columns=[
            'Film_title', 'Director','Cast', '½','★','★½','★★',
            '★★½','★★★','★★★½','★★★★','★★★★½','★★★★★','Film_URL', 'Studios', 'Spoken_languages'
            ])
        X_train = X_train[X_train.notna()]

        X_train = Preproc.clean_dataset(X_train)

        clf = IsolationForest(max_samples=len(X_train), random_state=0, n_estimators=len(X_train))
        clf.fit(X_train)
        
        decision_function = clf.decision_function(X_train).tolist()
        decision_function_copy = decision_function.copy()
        decision_function.sort()
        
        estimators = clf.estimators_

        df = pd.read_csv(path)
        df_values = df.values.tolist()
        columns = X_train.columns.to_list()
        anomalies = []
        
        for score in decision_function[0:anomaly_threshold]:
            index = decision_function_copy.index(score)
            decision_function_copy[index] = 1.0

            importances = estimators[index].feature_importances_
            list_importances = []
            for imp in importances:
                list_importances.append(imp)
            list_importances_copy = list_importances.copy()
            list_importances.sort(reverse=True)
            features = []
            for imp in list_importances[0:importance_threshold]:
                i = list_importances_copy.index(imp)
                list_importances_copy[i] = 0.0
                features.append(columns[i])

            anomalies.append(df_values[index]+[[features]])

        anomalies = pd.DataFrame(anomalies, columns=(df.columns.to_list() + ["importances"]))
        anomalies = anomalies[['Film_title', 'Release_year', 'importances']]

        print(anomalies)

        anomalies.to_csv("./data/anomalies_"+type+".csv", index=False)

