import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import statsmodels.formula.api as smf
import seaborn as sns; sns.set()

from preproc import Preproc

class Statistic:

    def correlations():

        path = "./data/data_lr.csv"

        df = pd.read_csv(path)
        
        df = df.drop(columns=[
            'Film_title', 'Director','Cast', '½','★','★½','★★',
            '★★½','★★★','★★★½','★★★★','★★★★½','★★★★★','Film_URL', 'Studios', 'Spoken_languages',
            'Genres', 'Countries', 'Original_language', 'Female_director_count','Male_director_count'
            ])

        df = df[df.notna()]
        # df = df[~(df == 0).any(axis=1)]

        # df_corr = df.corr()
        # df_corr = df_corr.style.background_gradient(cmap='RdBu')

        df_corr = sns.heatmap(df.corr())

        return df_corr
        # plt.matshow(df.corr())
        # plt.show()  

        # print(df_corr)


    def linear_regression(target):

        print("["+target+"]")

        path = "./data/data_lr.csv"

        df = pd.read_csv(path)
        
        # Tirando itens que não tem muito a ver, ou que são categóricos demais
        df = df.drop(columns=[
            'Film_title', 'Director','Cast', 
            '½',
            '★','★½','★★',
            '★★½','★★★','★★★½','★★★★','★★★★½'
            ,'★★★★★'
            ,'Film_URL', 'Studios', 
            'Spoken_languages'
            ,'Genres'
            , 'Countries'
            , 'Original_language'#, 'Female_director_count','Male_director_count'
            ])
        
        # Tirando itens para evitar multicolinearidade
        df = df.drop(columns=
            [
            'Average_rating'
             ,'Watches'
             ,'Female_director_count'
             ,'Male_director_count'
             , 'Likes'
             , 'Total_ratings'
             ,'List_appearances'
             , 'Fans'
             ,'Release_year'

            #  ,'Runtime'
            #  , 'Male_actor_count'
            #  ,'Female_actor_count'
            #  , 'victories_count'
            #  ,'indications_count'
            ])
        
        df = df[df.notna()]
        # df = df[~(df == 0).any(axis=1)]
        
        drop_list = [
            # 'Curitiba_average_rating'
            # ,
            # 'Curitiba_watches'#, 
            # ,
            'Curitiba_ratings'
            ]
        # drop_list.remove(target)

        df = df.drop(columns=drop_list)

        X = df.columns.to_list()
        X.remove(target)
        X_string = " + ".join(X)
        X_string = X_string.replace('Female_director_count', "C(Female_director_count)")
        X_string = X_string.replace('Male_director_count', "C(Male_director_count)")

        print(X_string)

        model = smf.ols(target + " ~ " + X_string, data=df)
        # model = smf.ols("Renda_Média ~ C(Nível_Urbanização) + Taxa_Analfabetismo + Área_Verde_m2_percapta + Área_Verde_km2 + Estabelecimentos_Ativos_Indústria + Estabelecimentos_Ativos_Comércio + Estabelecimentos_Ativos_Serviço + População_Total + Área_hectare + Área_km2 + Densidade_Demográfica_Hab_por_ha + Área_Verde_pct", data=df)
        response = model.fit()
        print(response.summary())

    def hello():
        print("Hello world!")

