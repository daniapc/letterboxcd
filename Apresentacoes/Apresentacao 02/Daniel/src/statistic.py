import pandas as pd
from preproc import Preproc
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
import numpy as np
class Statistic:

    def linear_regression(target, colour):
        from sklearn.linear_model import LinearRegression

        print("["+target+"]")

        path = "./data/data_lr.csv"

        df = pd.read_csv(path)
        
        X = df.drop(columns=[
            'Film_title', 'Director','Cast', '½','★','★½','★★',
            '★★½','★★★','★★★½','★★★★','★★★★½','★★★★★','Film_URL', 'Studios', 'Spoken_languages',
            'Genres', 'Countries', 'Original_language', 'Female_director_count','Male_director_count'
            ])
        
        X = X[X.notna()]
        X = X[~(X == 0).any(axis=1)]
        y = X[target]
        X = X.drop(columns=['Curitiba_average_rating', 'Curitiba_watches', 'Curitiba_ratings'])

        features = X.columns.to_list()

        for feature in features:
            print(feature+":")
            X_train, X_test, y_train, y_test = train_test_split(
                X[feature].values.reshape(len(X), 1)
                , y, test_size=0.2, random_state=0)

            reg = LinearRegression().fit(X_train, y_train)
            y_pred = reg.predict(X_test)

            coeficients = reg.coef_

            plt.scatter(X_test, y_test, color="black")
            plt.plot(X_test, y_pred, color=colour, linewidth=3)

            plt.show()

            print("coef:"+str(coeficients[0]))